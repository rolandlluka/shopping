<?php


namespace App\Http\Controllers;
use App\User;
use Datatable;
use yajra\Datatables\Datables;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Session;

class AdminController extends Controller
{

    public function getSignin()
    {
        return view('admin.signin');
    }

    public function postSignin(request $request)
    {
        $this->validate($request, [
            'email' => 'email|required',
            'password' => 'required|min:4'
        ]);
        if (Auth::attempt(['email'=>$request->input('email'), 'password'=>$request->input('password')])){
            if (Session::has('oldUrl')){
                $oldUrl = Session::get('oldUrl');
                Session::forget('oldUrl');
                return redirect()->to($oldUrl);
                if (Auth::user()->admin == 0) {
                    return redirect('admin/signin');
                }
            }
            return redirect()->route('admin.dashboard');
        }
        return redirect()->back();
    }
    public function getDashboard(){
        
        return view('admin.dashboard');

    }

        public function getLogout() {
            Auth::logout();
            return redirect()->route('admin.signin');
        }

    public function getAllUsers(){
        return view('admin.datatables');
    }
     public function showAllUsers(){
        return Datatable::collection(User::all())
        ->searchColumns('id', 'email')
        ->orderColumns('id', 'email')
        ->addColumn('email', function($user) {
            return $user->email;
        })
        ->addColumn('created_at', function($user) {
            return $user->created_at;
        })
        ->make(true);
    }

}
