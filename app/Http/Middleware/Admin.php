<?php

namespace App\Http\Middleware;

use Closure;  
use Illuminate\Support\Facades\Auth;

class Admin  
{
  public function handle($request, Closure $next, $guard = null)
  {
    if (Auth::guard($guard)->guest()) {
      if ($request->ajax()) {
        return response('Unauthorized.', 401);
      } else {
        return redirect()->guest('admin/signin');
      }
    } else if (!Auth::guard($guard)->user()->admin != 0) {
      return redirect()->to('/admin/signin')->withError('Permission Denied');
    }

    return $next($request);
  }
}