<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

 
/*Route::get('datatables', function () {
         return view('/user.datatables');
        });

        Route::get('serverSide', [
            'as'   => 'user.serverSide',
            'uses' => function () {
            $users = User::select(['id', 'email', 'created_at']);
 
             return Datatables::of($users)->make();
    }
]);
*/

 
Route::get('/', [
    'uses' => 'ProductController@getIndex',
    'as' => 'product.index'
]);

Route::get('/add-to-cart/{id}', [
    'uses' => 'ProductController@getAddToCart',
    'as' => 'product.addToCart'
]);

Route::get('/reduce/{id}',[
   'uses' => 'ProductController@getReduceByOne',
    'as' => 'product.reduceByOne'
]);

Route::get('/remove/{id}', [
   'uses' => 'ProductController@getRemoveItem',
    'as' => 'product.remove'
]);
Route::get('/shopping-cart', [
    'uses' => 'ProductController@getCart',
    'as' => 'product.shoppingCart'
]);

Route::get('/checkout', [
   'uses' => 'ProductController@getCheckout',
    'as' => 'checkout',
    'middleware' => 'auth'
]);

Route::post('/checkout', [
   'uses' => 'ProductController@postCheckout',
    'as' => 'checkout',
    'middleware' => 'auth'
]);
Route::group(['prefix'=>'user'], function (){
    Route::group(['middleware'=>'guest'], function (){
        Route::get('/signup', [
            'uses' => 'UserController@getSignup',
            'as' => 'user.signup'
        ]);
    Route::post('/signup', [
            'uses' => 'UserController@postSignup',
            'as' => 'user.signup'
        ]);

        Route::get('/signin', [
            'uses' => 'UserController@getSignin',
            'as' => 'user.signin'
        ]);

        Route::post('/signin', [
            'uses' => 'UserController@postSignin',
            'as' => 'user.signin'
        ]);
    });
        Route::group(['middleware'=>'auth'], function (){
            Route::get('profile', [
                'uses' => 'UserController@getProfile',
                'as' => 'user.profile'
            ]);
            Route::get('logout', [
                'uses' => 'UserController@getLogout',
                'as' => 'user.logout'
            ]);
      });
    });

Route::group(['prefix'=>'admin'], function (){
    Route::group(['middleware'=>'guest'], function (){

        Route::get('datatables', [
        'uses' => 'AdminController@getAllUsers',
        'as' => 'allUsers'
        ]);
 
        Route::get('dtb', [
            'uses' => 'AdminController@showAllUsers',
            'as' => 'getAllUsers'
        ]);
                
        Route::get('/signin', [
            'uses' => 'AdminController@getSignin',
            'as' => 'admin.signin'
        ]);

        Route::post('/signin', [
            'uses' => 'AdminController@postSignin',
            'as' => 'admin.signin'
        ]);
    });
        Route::group(['middleware'=> 'admin'] , function (){
            Route::get('dashboard', [
                'uses' => 'AdminController@getDashboard',
                'as' => 'admin.dashboard',

            ]);
            Route::get('logout', [
                'uses' => 'AdminController@getLogout',
                'as' => 'admin.logout'
            ]);
      });
    });


