<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new \App\Product([
            'imagePath' => 'http://universe.byu.edu/wp-content/uploads/2015/01/HP4cover.jpg',
            'title' => 'Harry Potter',
            'description' => 'Super cool - at least as a child',
            'price' => 20
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'http://universe.byu.edu/wp-content/uploads/2015/01/HP4cover.jpg',
            'title' => 'Blla',
            'description' => 'YEs we can',
            'price' => 20
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'http://universe.byu.edu/wp-content/uploads/2015/01/HP4cover.jpg',
            'title' => 'Harry Potter',
            'description' => 'Super cool - at least as a child',
            'price' => 20
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'http://universe.byu.edu/wp-content/uploads/2015/01/HP4cover.jpg',
            'title' => 'Harry Potter',
            'description' => 'Super cool - at least as a child',
            'price' => 20
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'http://universe.byu.edu/wp-content/uploads/2015/01/HP4cover.jpg',
            'title' => 'Harry Potter',
            'description' => 'Super cool - at least as a child',
            'price' => 20
        ]);
        $product->save();
    }
}
