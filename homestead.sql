-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 19, 2016 at 03:54 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homestead`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_08_16_082843_create_products_table', 1),
('2016_08_16_125435_create_users_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `imagePath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `created_at`, `updated_at`, `imagePath`, `title`, `description`, `price`) VALUES
(1, '2016-08-16 10:37:26', '2016-08-16 10:37:26', 'http://universe.byu.edu/wp-content/uploads/2015/01/HP4cover.jpg', 'Harry Potter', 'Super cool - at least as a child', 20),
(2, '2016-08-16 10:37:26', '2016-08-16 10:37:26', 'http://universe.byu.edu/wp-content/uploads/2015/01/HP4cover.jpg', 'Blla', 'YEs we can', 20),
(3, '2016-08-16 10:37:26', '2016-08-16 10:37:26', 'http://universe.byu.edu/wp-content/uploads/2015/01/HP4cover.jpg', 'Harry Potter', 'Super cool - at least as a child', 20),
(4, '2016-08-16 10:37:26', '2016-08-16 10:37:26', 'http://universe.byu.edu/wp-content/uploads/2015/01/HP4cover.jpg', 'Harry Potter', 'Super cool - at least as a child', 20),
(5, '2016-08-16 10:37:26', '2016-08-16 10:37:26', 'http://universe.byu.edu/wp-content/uploads/2015/01/HP4cover.jpg', 'Harry Potter', 'Super cool - at least as a child', 20);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `created_at`, `updated_at`, `email`, `password`, `remember_token`) VALUES
(1, '2016-08-18 06:58:04', '2016-08-18 10:53:05', 'test@gmail.com', '$2y$10$4gFWvYSO0YXeg8keM/4uqO0AEDTYhWPAy50Z8Z/yRvs45V.Cq1KFW', 'nnQb8tEMTKRuKxuFgs5uMI8vOgAj40eajriHPUF0OcPRNF0zVPJtROS7YwMl'),
(2, '2016-08-18 07:22:45', '2016-08-18 07:22:50', 'test2@gmail.com', '$2y$10$tr2GSFS1d1lNcyrsBIvMrO5mU0U0S5hUtEWY16FeyDovLAG.rybWy', 'Jm5Zk0x1lmMsaHnFl89JKrtrH6ARXUK54d6dx45YPgNvGNQI5kOUKcmsSk8n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
