var oTable=null;

$(document).ready(function()
{
    $('#allUsers').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.datatabless') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'email', name: 'email' },
            { data: 'created_at', name: 'created_at' },
            
        ]
    });
});