<!DOCTYPE html>

<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{URL::to('src/js/jquery.min.js')}}"></script>
        <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/plug-ins/9323ccdd67/integration/bootstrap/3/dataTables.bootstrap.js"></script>

</head>
<body style="font-family: 'Segoe UI' ">
 <nav class="navbar navbar-inverse" role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle NAvigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#" style="color: white">Datatables</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        
        <li><a href="#">Page 1</a></li>
        <li><a href="{{route('admin.logout')}}">Page 2</a></li>
        <li><a href="#">Page 3</a></li>
      </ul>
    </div>
</nav>
  <br>
  <br>
  <div class="container">
  <br>
  <div class="table-responsive col-lg-8">

  <table id="allUsers" class="table table-condensed table-hover" cellspacing="0" width="100%">
  <thead>
      <tr>
        <th>ID</th>
        <th>Email</th>
        <th>Created at</th>
      </tr>
    </thead>
    <tbody>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      </tr>
    </tbody>
    </table>
  </div>
  </div>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{URL::to('src/js/datatables.js')}}"></script>

</body>
</html>

@push('scripts')
<script>
$(function() {
    $('#allUsers').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'http://laravel.dev/admin/datatables',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'email', name: 'email' },
            { data: 'created_at', name: 'created_at' },
        ]
    });
});
</script>
@endpush

